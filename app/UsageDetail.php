<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsageDetail extends Model
{
    protected $table = 'usage_detail';
    const CREATED_AT = null;
    const UPDATED_AT = null;
}