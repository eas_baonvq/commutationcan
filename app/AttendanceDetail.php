<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceDetail extends Model
{
    protected $table = 'attendance_detail';
    const CREATED_AT = null;
    const UPDATED_AT = null;
}
