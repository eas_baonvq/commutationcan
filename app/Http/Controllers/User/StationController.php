<?php

namespace App\Http\Controllers\User;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\File;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\Models\UsageDetail;


class StationController extends Controller
{
    public function index(Request $request)
    {
        return view('user.station');
    }

    public function get()
    {
        $userStations = \DB::table('user_station')
                            ->where('user_id', \Auth::id())
                            ->get();
        return response($userStations->jsonSerialize(), Response::HTTP_OK);
    }

    public function add(Request $request, $station, $type)
    {
        \DB::table('user_station')
            ->insert(['user_id' => \Auth::id(), 'station' => $station, 'type' => $type]);
        return response(null, Response::HTTP_OK);
    }

    public function update(Request $request, $id,  $station, $type)
    {
        \DB::table('user_station')
            ->where('id', $id)
            ->update(['station' => $station, 'type' => $type]);
        return response(null, Response::HTTP_OK);
    }

    public function delete(Request $request, $id)
    {
        \DB::table('user_station')
            ->where('id', $id)
            ->delete();
        return response(null, Response::HTTP_OK);
    }
}
