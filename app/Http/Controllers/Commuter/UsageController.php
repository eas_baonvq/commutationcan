<?php

namespace App\Http\Controllers\Commuter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Commuter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;


class UsageController extends Controller
{
    public function submitForm(Request $request)
    {
        return view('commuter.usage-record');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request)
    {
        $csv = $request->file('usage_record_file');
        $lines = fopen($csv, "r");

        while($line = fgetcsv($lines)) {
            mb_convert_variables('UTF-8', 'SJIS-win', $line);
            if (strpos($line[0],'日付') !== false) {
                continue;
            }
            if (strpos($line[2],'-') === false) {
                continue;
            }
            $route = $line[1];
            if (strpos($route,'入') !== false && strpos($route,'定') !== false ) {
                $route = str_replace('定', '出', $route);
            } else if (strpos($route,'出') !== false && strpos($route,'定') !== false ) {
                $route = str_replace('定', '入', $route);
            } else if (strpos($route,'入') === false && strpos($route,'出') === false ) {
                continue;
            }
            $station = explode("出", trim(explode("入", $route)[1]));
            if (strpos($station[0],'(') !== false) {
                $from = strstr(trim($station[0]),'(',true);
                $to = strstr(trim($station[1]),'(',true);
            } else {
                $from = trim($station[0]);
                $to = trim($station[1]);
            }
            $usageDetail= new \App\UsageDetail;
            $usageDetail->user_id = \Auth::id();
            $usageDetail->date = $line[0];
            $usageDetail->from = $from;
            $usageDetail->to = $to;
            $cost = str_replace('-', '', $line[2]);
            $usageDetail->cost = $cost;
            $usageDetail->save();
        }
        fclose($lines);
        return back()->with('flash_message','CSVのデータを読み込みました。');
    }
}
