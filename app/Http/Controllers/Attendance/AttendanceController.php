<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Attendance;
use Illuminate\Database\Eloquent\Model;


class AttendanceController extends Controller
{
    public function submitForm(Request $request)
    {
        $users = \DB::table('users')->get();
        return view('attendance.attendance-record',["users" => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request)
    {
        // attendance_recordへ登録し、登録したIDを取得
        $userId = $request->input('user_id');
        $month = $request->input('month');
        $attendanceRecordId =\App\AttendanceRecord::where('user_id', $userId)
            ->where('month', $month)
            ->select('id')
            ->first();

        if (empty( $attendanceRecordId )) {
            $attendanceRecord = new \App\AttendanceRecord;
            $attendanceRecord-> user_id = $userId;
            $attendanceRecord-> month = $month;
            $attendanceRecord->save();

            $attendanceRecordId =\App\AttendanceRecord::where('user_id', $userId)
            ->where('month', $month)
            ->select('id')
            ->first();
        }
        // attendance_detailへ登録
        $csv = $request->file('attendance_record_file');
        $lines = fopen($csv, "r");

        while($line = fgetcsv($lines)) {
            if (strpos($line[0],'日付')!== false) {
                continue;
            }
            if(empty ($line[4])) {
                continue;
            }
            $attendanceDetail = new \App\AttendanceDetail;
            $attendanceDetail->attendance_record_id = $attendanceRecordId -> id;
            $attendanceDetail->date = $line[0];
            $attendanceDetail->checkin = $line[4];
            $attendanceDetail->checkout = $line[5];
            $attendanceDetail->save();
        }
        fclose($lines);
        return back()->with('flash_message','CSVのデータを読み込みました。');
    }
}
