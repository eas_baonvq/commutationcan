<?php

namespace App\Http\Controllers\Application;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\File;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\UsageDetail;


class ApplicationController extends Controller
{
    public function index(Request $request)
    {
        $applications = \DB::table('application')->get();
        return view('application.index', ["applications" => $applications]);
    }

    public function createForm(Request $request)
    {
        $users = \DB::table('users')->get();
        $attendanceRecords = \DB::table('attendance_record')->get();
        return view('application.create', ["users" => $users, "attendanceRecords" => $attendanceRecords]);
    }

    public function create(Request $request)
    {
        $userId = $request->input('user_id');
        $attendanceRecordId = $request->input('attendance_record_id');
        $attendanceRecords = \DB::table('attendance_record')->where('id', $attendanceRecordId)->first();
        $workUsageDates = \DB::table('attendance_detail')
                            ->where('attendance_record_id', $attendanceRecordId)
                            ->select('date')
                            ->get()->toArray();
        $usageDetails = \DB::table('usage_detail')
                            ->whereIn('date', array_column($workUsageDates, 'date'))
                            ->orderByDesc('id') //('date')
                            ->get()->toArray();

        $homeStations = \DB::table('user_station')->where('user_id', \Auth::id())->where('type', 'home')->get()->toArray();
        $customerStations = \DB::table('user_station')->where('user_id', \Auth::id())->where('type', 'customer')->get()->toArray();
        $companyStations = \DB::table('user_station')->where('user_id', \Auth::id())->where('type', 'company')->get()->toArray();
        $transferStations = \DB::table('user_station')->where('user_id', \Auth::id())->where('type', 'transfer')->get()->toArray();
        $targetStations = array_column(array_merge($homeStations, $customerStations, $companyStations), 'station');
        $transferStations = array_column($transferStations, 'station');

        $applyingUsages = [];
        $i = 0;
        while (
            $i < count($usageDetails)
        ) {
            Log::debug($usageDetails[$i]-> from); Log::debug($usageDetails[$i]-> to);
            if (array_search($usageDetails[$i]->from, $targetStations) !== false){
                if (array_search($usageDetails[$i]->to, $targetStations) !== false) {
                    $applyingUsages[] = $usageDetails[$i];
                } else if (array_search($usageDetails[$i]->to, $transferStations) !== false) {
                    $tempApplyingUsages = [];
                    $j = $i;
                    while (
                        $j < count($usageDetails)
                        && $usageDetails[$j + 1]->from == $usageDetails[$j]->to
                    ) {
                        $tempApplyingUsages[] = $usageDetails[$j];
                        $i++;

                        if (array_search($usageDetails[$j + 1]->to, $targetStations) !== false) {
                            $tempApplyingUsages[] = $usageDetails[$j + 1];
                            break;
                        } else if (array_search($usageDetails[$j + 1]->to, $transferStations) !== false) {
                            $j++;
                        } else {
                            $tempApplyingUsages = [];
                            break;
                        }
                    }
                    $applyingUsages = array_merge($applyingUsages, $tempApplyingUsages);
                }
            }
            $i++;
        }
        $applicationId = \DB::table('application')->insertGetId (['user_id' => $userId, 'attendance_record_id' => $attendanceRecordId, 'month' => $attendanceRecords->month, 'status' => '未承認']);
        \DB::table('usage_detail')
            ->whereIn('id', array_column($applyingUsages, 'id'))
            ->update(['application_id' => $applicationId]);
        return redirect('application/'.$applicationId);
    }

    public function detail($applicationId)
    {
        $application = \DB::table('application')->where('id', $applicationId)->first();
        return view('application.detail', ["application" => $application]);
    }

    public function delete($applicationId)
    {
        \DB::table('application')->where('id', $applicationId)->delete();
        return redirect('/application/index');
    }

    public function update(Request $request, $applicationId)
    {
        \DB::table('application')
            ->where('id', $applicationId)
            ->update(['sum' => $request->sum]);
        return response(null, Response::HTTP_OK);
    }

    public function getUsages($applicationId)
    {
        $application = \DB::table('application')->where('id', $applicationId)->first();
        $usageDetails = \DB::table('usage_detail')
                            ->where('user_id', \Auth::id())
                            ->whereDate('date', '>=', date($application->month . '-01'))
                            ->whereDate('date', '<', date('Y-m-d', strtotime($application->month . '-01'. ' + 1 month')))
                            ->orderByDesc('id')
                            ->get();
        return response($usageDetails->jsonSerialize(), Response::HTTP_OK);
    }

    public function updateUsage(Request $request, $applicationId, $usageId)
    {
        \DB::table('usage_detail')
            ->where('id', $usageId)
            ->update(['application_id' => !$request->isApplicated ? $applicationId : null]);
        return response(null, Response::HTTP_OK);
    }

    public function deleteUsage(Request $request, $applicationId, $usage_id)
    {
        \DB::table('usage_detail')
            ->where('id', $usage_id)
            ->delete();
        return response(null, Response::HTTP_OK);
    }

    public function exportExcel(Request $request, $applicationId)
    {
        $application = \DB::table('application')->where('id', $applicationId)->first();
        $usageDetails = \DB::table('usage_detail')->where('application_id', $applicationId)->orderByDesc('id')->get()->toArray();
        $spreadsheet = IOFactory::load(public_path() . '/template/commuter_application.xlsx');
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('E10', \Auth::user()->name);
        $sheet->setCellValue('Q10', \Auth::user()->id);
        $sheet->setCellValue('X10', date('Y'));
        $sheet->setCellValue('AB10', date('n'));
        $sheet->setCellValue('AE10', date('j'));

        $firstDataRow = 16;
        foreach ($usageDetails as  $index => $usageDetail) {
            $sheet->setCellValue('E' . ($firstDataRow + $index), date('Y', strtotime($usageDetail->date)));
            $sheet->setCellValue('I' . ($firstDataRow + $index), date('n', strtotime($usageDetail->date)));
            $sheet->setCellValue('L' . ($firstDataRow + $index), date('j', strtotime($usageDetail->date)));
            $sheet->setCellValue('S' . ($firstDataRow + $index), $usageDetail->from);
            $sheet->setCellValue('X' . ($firstDataRow + $index), $usageDetail->to);
            $sheet->setCellValue('AD' . ($firstDataRow + $index), $usageDetail->cost);
        }
        $sheet->setCellValue('AD46', $application->sum);

        File::setUseUploadTempDirectory(public_path());

        $writer = new Xlsx($spreadsheet);
        $writer->save(public_path() . '/output.xlsx');

        return response()->download(public_path() . '/output.xlsx', \Auth::user()->name . '_' . $application->month . '_交通費精算書.xlsx',
                                        ['content-type' => 'application/vnd.ms-excel',])
                         ->deleteFileAfterSend(true);
    }
}
