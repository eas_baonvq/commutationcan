<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceRecord extends Model
{
    protected $table = 'attendance_record';
    const CREATED_AT = null;
    const UPDATED_AT = null;
}
