@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">勤怠データアップロード</div>

                <div class="card-body">
                    <!-- フラッシュメッセージ -->
                    @if (session('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session('flash_message') }}
                    </div>
                    @endif

                    <form action="/attendance/submit" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ユーザー：</label>
                            <select name="user_id" class="form-control col-sm-8">
                                @foreach ($users as $user)
                                    <option value="{{ $user -> id }}">{{ $user -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">年/月：</label>
                            <input class="form-control col-sm-8" type="month" name="month">
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">データ：</label>
                            <input type="file" class="form-control col-sm-8" value="ファイルを選択" name="attendance_record_file">
                        </div>
                        <button class="btn btn-primary" type="submit">インポート</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
