@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <!-- フラッシュメッセージ -->
                @if (session('flash_message'))
                    <div class="flash_message">
                        {{ session('flash_message') }}
                    </div>
                @endif

                <div class="card-header">Application</div>

                <div class="card-body">
                    <form action="/application/create" method="post">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">ユーザー：</label>
                            <select name="user_id" class="form-control col-sm-8">
                                @foreach ($users as $user)
                                <option value="{{ $user -> id }}">{{ $user -> name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">勤怠月：</label>
                            <select name="attendance_record_id"　class="form-control col-sm-8">
                                @foreach ($attendanceRecords as $attendanceRecord)
                                    <option value="{{ $attendanceRecord->id }}">{{ $attendanceRecord->month }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit">作成</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
