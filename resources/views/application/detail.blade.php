@extends('layouts.app')

@section('content')
<div id="mute"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <!-- フラッシュメッセージ -->
                @if (session('flash_message'))
                    <div class="flash_message">
                        {{ session('flash_message') }}
                    </div>
                @endif

                <div class="card-header">交通費利用詳細</div>

                <div class="card-body">
                    <application-component application-id="{{ $application->id }}"></application-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
