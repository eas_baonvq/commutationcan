@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <!-- フラッシュメッセージ -->
                @if (session('flash_message'))
                    <div class="flash_message">
                        {{ session('flash_message') }}
                    </div>
                @endif

                <div class="card-header">交通費申請</div>

                <div class="card-body">
                    <div class="table-responsive-xl">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <!-- <th scope="col">ユーザーID</th>-->
                            <th scope="col">月</th>
                            <th scope="col">合計</th>
                            <th scope="col">ステータス</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($applications as $application)
                        <tr>
                            <th scope="row">{{ $loop->index + 1 }}</th>
                            <!-- <td>{{ $application->user_id }}</td> -->
                            <td>{{ $application->month }}</td>
                            <td>{{ $application->sum }}</td>
                            <td>{{ $application->status }}</td>
                            <td><a class="btn btn-success" href="/application/{{$application->id}}" role="button">詳細</a>
                                <a class="btn btn-danger" href="/application/{{$application->id}}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                    {{ __('削除') }}
                                </a>
                                <form id="delete-form" action="/application/{{$application->id}}/delete" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <a class="btn btn-primary" href="/application/create">作成</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
