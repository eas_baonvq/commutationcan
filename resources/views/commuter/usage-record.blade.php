@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">利用明細アップロード</div>

                <div class="card-body">
                    <!-- フラッシュメッセージ -->
                    @if (session('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {{ session('flash_message') }}
                    </div>
                    @endif
                    <form action="/commuter/usage/submit" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">データ：</label>
                            <input type="file" class="form-control col-sm-8" value="ファイルを選択" name="usage_record_file">
                        </div>
                        <button class="btn btn-primary" type="submit">インポート</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
