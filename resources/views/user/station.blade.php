@extends('layouts.app')

@section('content')
<div id="mute"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <!-- フラッシュメッセージ -->
                @if (session('flash_message'))
                    <div class="flash_message">
                        {{ session('flash_message') }}
                    </div>
                @endif

                <div class="card-header">駅</div>

                <div class="card-body">
                    <user-station-component></user-station-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
