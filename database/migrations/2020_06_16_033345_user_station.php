<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_station', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->text('station');
            $table->enum('type', ['home', 'company', 'customer', 'transfer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_station');
    }
}
