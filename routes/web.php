<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//jobcan用
Route::group(['namespace' => 'Attendance'], function() {
    Route::get('/attendance/submit', 'AttendanceController@submitForm')->name('attendance/submit');
    Route::post('/attendance/submit', 'AttendanceController@submit');
});

//利用明細用
Route::group(['namespace' => 'Commuter'], function() {
    Route::get('/commuter/usage/submit', 'UsageController@submitForm')->name('commuter/usage/submit');
    Route::post('/commuter/usage/submit', 'UsageController@submit');
});

Route::group(['namespace' => 'Application'], function() {
    Route::get   ('/application/index', 'ApplicationController@index')->name('application/index');
    Route::get   ('/application/create', 'ApplicationController@createForm');
    Route::post  ('/application/create', 'ApplicationController@create');
    Route::get   ('/application/{applicationId}', 'ApplicationController@detail');
    Route::post  ('/application/{applicationId}/delete', 'ApplicationController@delete');
    Route::put   ('/api/application/{applicationId}', 'ApplicationController@update');
    Route::get   ('/api/application/{applicationId}/usage', 'ApplicationController@getUsages');
    Route::put   ('/api/application/{applicationId}/usage/{usageId}', 'ApplicationController@updateUsage');
    Route::delete('/api/application/{applicationId}/usage/{usageId}', 'ApplicationController@deleteUsage');
    Route::get   ('/api/application/{applicationId}/export', 'ApplicationController@exportExcel');
});

Route::group(['namespace' => 'User'], function() {
    Route::get   ('/user/station', 'StationController@index')->name('user/station');
    Route::get   ('/api/user/station', 'StationController@get');
    Route::post  ('/api/user/station', 'StationController@add');
    Route::put   ('/api/user/station/{id}', 'StationController@update');
    Route::delete('/api/user/station/{id}', 'StationController@delete');
});
